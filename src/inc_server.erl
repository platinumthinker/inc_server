-module(inc_server).

-behaviour(gen_server).

-export([
         code_change/3,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         init/1,
         next_value/0,
         start_link/0,
         stop/0,
         terminate/2
        ]).


-record(state, {
          id = 0 :: non_neg_integer()
         }).

start_link() ->
    gen_server_cluster:start_link(?MODULE, ?MODULE, [], []).

next_value() ->
    gen_server:call({global, ?MODULE}, nextValue).

stop() ->
    gen_server:call({global, ?MODULE}, stop).

init([]) ->
    io:format("Start inc_server~n"),
    net_kernel:monitor_nodes(true),
    {ok, #state{}}.

handle_call(nextValue, _From, State) ->
    Value = State#state.id,
    {reply, Value, State#state{id = Value + 1}};

handle_call(stop, _From, State) ->
    {stop, normalStop, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info({nodeup, Node}, State) ->
    io:format("[~p] Node ~p added to the cluster.", [?MODULE, Node]),
    {noreply, State};
handle_info({nodedown, Node}, State) ->
    io:format("[~p] Node ~p exited from the cluster.", [?MODULE, Node]),
    {noreply, State};
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
