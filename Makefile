.PHONY: all compile release

all: compile release

compile:
	rebar compile

release:
	relx
