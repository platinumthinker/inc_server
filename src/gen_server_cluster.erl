-module(gen_server_cluster).

-behaviour(gen_server).

-export([
         start/4,
         start_link/4,
         start_local_server/1,
         get_target_state/1,
         get_all_server_pids/1,
         get_all_server_nodes/1,
         is_running/1,
         stop/2,
         link/2,
         unlink/2
        ]).

-export([
         init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3
        ]).

-record(state, {
          name,
          globalServerPid,
          localServerPidList = [],
          targetLinkedPidList = [],
          targetModule,
          targetState
         }).


-spec start(Name :: module(), TargetModule :: module(),
            Args :: [], Options :: []) ->
    noGlobalServerRunning | {ok, pid()} | {error, _Reason}.
start(Name, TargetModule, TargetArgs, Options) ->
    ArgsGlobalInit = {initGlobal, Name, TargetModule, TargetArgs},
    case gen_server:start({global, Name}, ?MODULE, ArgsGlobalInit, Options) of
        {ok, _GlobalServerPid} = Result ->
            io:format("[~p] ~p started as global server.~n", [?MODULE, Name]),
            Result;
        {error, {already_started, _GlobalServerPid}} ->
            start_local_server(Name)
    end.

-spec start_link(Name :: module(), TargetModule :: module(),
            Args :: [], Options :: []) ->
    noGlobalServerRunning | {ok, pid()} | {error, _Reason}.
start_link(Name, TargetModule, TargetArgs, Options) ->
    ArgsGlobalInit = {initGlobal, Name, TargetModule, TargetArgs},
    case gen_server:start_link({global, Name}, ?MODULE, ArgsGlobalInit, Options) of
        {ok, _GlobalServerPid} = Result ->
            io:format("[~p] ~p started as global server. ~n", [?MODULE, Name]),
            Result;
        {error, {already_started, _GlobalServerPid}} ->
            start_local_server(Name)
    end.

-spec start_local_server(Name :: module()) ->
    noGlobalServerRunning | {ok, pid()} | {error, _Reason}.
start_local_server(Name) ->
    case is_running(Name) of
        false ->
            noGlobalServerRunning;
        true ->
            ArgsLocalInit = {initLocal, Name},
            case gen_server:start({local, Name}, ?MODULE, ArgsLocalInit, []) of
                {ok, _LocalPid} = Result ->
                    io:format("[~p] ~p started as local server. ~n", [?MODULE,Name]),
                    Result;
                Else ->
                    Else
            end
    end.

-spec get_all_server_pids(Name :: atom()) -> {pid(), [pid()]}.
get_all_server_pids(Name) ->
    gen_server:call({global, Name}, get_all_server_pids).

-spec get_all_server_nodes(Name :: atom()) -> {node(), [node()]}.
get_all_server_nodes(Name) ->
    {GlobalServerPid, LocalServerPidList} = get_all_server_pids(Name),
    Fun = fun(Pid) -> node(Pid) end,
    {Fun(GlobalServerPid), lists:map(Fun, LocalServerPidList)}.

-spec is_running(Name :: atom()) -> boolean().
is_running(Name) ->
    case catch get_all_server_pids(Name) of
        {Pid, PidList} when is_pid(Pid), is_list(PidList) ->
            true;
        _ ->
            false
    end.

stop(Name, global) ->
    gen_server:call({global, Name}, {stopByGenServerCluster, stopGlobalServer});

stop(Name, all) ->
    {_, LocalServerNodeList} = get_all_server_nodes(Name),
    Request = {stopByGenServerCluster, stopAllServer},
    case LocalServerNodeList of
        [] ->
            ok;
        _ ->
            gen_server:multi_call(LocalServerNodeList, Name, Request)
    end,
    gen_server:call({global, Name}, Request);

stop(Name, Node) ->
    gen_server:multi_call([Node], Name, {stopByGenServerCluster, stopLocalServer}).

%% Returns the target state.
get_target_state(Name) ->
    gen_server:call({global, Name}, get_target_state).

link(Name, Pid) ->
    gen_server:cast({global, Name}, {link, Pid}).

unlink(Name, Pid) ->
    gen_server:cast({global, Name}, {unlink, Pid}).


init({initGlobal, Name, TargetModule, TargetArgs}) ->
    process_flag(trap_exit, true),
    %% initialize callback server:
    io:format("[~p] initialize callback server: ~p, arguments: ~p ~n",
               [?MODULE,TargetModule,TargetArgs]),
    TargetResult = TargetModule:init(TargetArgs),
    case TargetResult of
        {ok, TargetState} ->
            State = #state{name = Name, globalServerPid = self(),
                           targetModule = TargetModule, targetState = TargetState},
            {ok, State};
        {ok, TargetState, _Timeout} ->
            State = #state{name = Name, globalServerPid = self(),
                           targetModule = TargetModule, targetState = TargetState},
            {ok, State};
        {stop, Reason} ->
            {stop, Reason};
        ignore ->
            ignore
    end;

%% Called by local server at startup.
init({initLocal, Name}) ->
    process_flag(trap_exit, true),
    State = gen_server:call({global, Name}, init_local_server_state),
    {ok, State}.

handle_call(get_target_state, _From, State) ->
    Reply = State#state.targetState,
    {reply, Reply, State};

%% Called by global server to get the pids of the global and all local servers.
handle_call(get_all_server_pids, _From, State) ->
    Reply = {State#state.globalServerPid, State#state.localServerPidList},
    {reply, Reply, State};

handle_call(init_local_server_state, {FromPid, _}, State) ->
    AddNewLocalServerFun =
        fun(S) ->
            S#state{localServerPidList = [FromPid | S#state.localServerPidList]}
        end,
    NewState = update_all_server_state(State, AddNewLocalServerFun),
%% link to this local server:
    link(FromPid),
    Reply = NewState,
    {reply, Reply, NewState};

%% Called by global or local server due to a client stop request.
handle_call({stopByGenServerCluster, Reason} = Reply, _From, State) ->
    {stop, Reason, Reply, State};

%% Called by local server due to a target stop request.
handle_call(stopByTarget = Reason, _From, State) ->
    {stop, Reason, State};

%% Called by global or local server and delegates the request to the target.
handle_call(Request, From, State) ->
    delegate_to_target(State, handle_call, [Request, From,
        State#state.targetState]).


handle_cast({update_local_server_state, UpdateFun}, State) ->
    NewState = UpdateFun(State),
    {noreply, NewState};

handle_cast({link, Pid}, State) ->
%% function to update state by storing new pid linked to target:
    AddNewTargetLinkedPidFun =
        fun(S) ->
%% remove Pid first to avoid duplicates:
            L = lists:delete(Pid, S#state.targetLinkedPidList),
            S#state{targetLinkedPidList = [Pid | L]}
        end,
    NewState = update_all_server_state(State, AddNewTargetLinkedPidFun),
%% link to this local server:
    link(Pid),
    {noreply, NewState};

handle_cast({unlink, Pid}, State) ->
%% function to update state by removing link to pid:
    RemoveTargetLinkedPidFun =
        fun(S) ->
            S#state{targetLinkedPidList
            = lists:delete(Pid, S#state.targetLinkedPidList)}
        end,
    NewState = update_all_server_state(State, RemoveTargetLinkedPidFun),
%% unlink to this local server:
    unlink(Pid),
    {noreply, NewState};


%% Called by global or local server and delegates the request to the target.
handle_cast(Msg, State) ->
    delegate_to_target(State, handle_cast, [Msg, State#state.targetState]).

handle_info({'EXIT', Pid, Reason}, State) when Pid == State#state.globalServerPid ->
    io:format("Global server on ~p terminated: ~p.~n", [node(Pid), Reason]),
    NewGlobalServerPid = try_register_as_global_server(State#state.name, Pid),
    io:format("New global server on ~p.~n", [node(NewGlobalServerPid)]),
%% update new global server pid and local server pid list:
    NewLocalServerPidList = lists:delete(NewGlobalServerPid,
        State#state.localServerPidList),
    NewState = State#state{globalServerPid = NewGlobalServerPid,
    localServerPidList = NewLocalServerPidList},
    case NewGlobalServerPid == self() of
        true ->
            %% link to all local servers:
            F = fun(P) ->
                link(P)
            end,
            lists:foreach(F, NewLocalServerPidList),
            %% link to all pids the target was linked to:
            lists:foreach(F, NewState#state.targetLinkedPidList);
        false ->
            ok
    end,
    {noreply, NewState};

handle_info({'EXIT', Pid, _Reason} = Info, State) ->
    case lists:member(Pid, State#state.localServerPidList) of
        true ->
            %% Pid is a local server that has terminated:
            %% function to update state by deleting the local server pid:
            RemoveLocalServerFun =
                fun(S) ->
                    S#state{localServerPidList
                    = lists:delete(Pid, S#state.localServerPidList)}
                end,
            NewState = update_all_server_state(State, RemoveLocalServerFun),
            {noreply, NewState};
        false ->
            NewState = case lists:member(Pid, State#state.targetLinkedPidList) of
                true ->
                    %% Pid was linked to target, so remove from link list:
                    {reply, _, NewState1} = handle_call({unlink, Pid}, void, State),
                    NewState1;
                false ->
                    State
            end,
%% the target created the link to pid, thus delegate:
            delegate_to_target(NewState, handle_info,
                [Info, NewState#state.targetState])
    end;

%% Called by global or local server and delegates the info request to the target.
handle_info(Info, State) ->
    delegate_to_target(State, handle_info, [Info, State#state.targetState]).


terminate(Reason, State) ->
    TargetModule = State#state.targetModule,
    TargetModule:terminate(Reason, State#state.targetState),
    io:format("Server ~p stopped.~n", [State#state.name]).


code_change(OldVsn, State, Extra) ->
    io:format("[~p] Code changed.~n", [?MODULE]),
    io:format("[~p] OldVsn: ~p, State: ~p, Extra: ~p~n", [?MODULE, OldVsn, State, Extra]),
    {ok, State}.


%%--------------------------------------------------------------------
%%% Internal functions
%%--------------------------------------------------------------------
delegate_to_target(State, TargetCall, Args) ->
    TargetModule = State#state.targetModule,
    TargetResult = apply(TargetModule, TargetCall, Args),
    %% index of state in tuple:
    {IndexState, TargetStateUpdate} = case TargetResult of
        {reply, _Reply, TargetStateUpdate1} ->
            {3, TargetStateUpdate1};
        {reply, _Reply, TargetStateUpdate1, _Timeout} ->
            {3, TargetStateUpdate1};
        {noreply, TargetStateUpdate1} ->
            {2, TargetStateUpdate1};
        {noreply, TargetStateUpdate1, _Timeout} ->
            {2, TargetStateUpdate1};
        {stop, _Reason, _Reply, TargetStateUpdate1} ->
            {4, TargetStateUpdate1};
        {stop, _Reason, TargetStateUpdate1} ->
            {3, TargetStateUpdate1};
        {ok, TargetStateUpdate1} -> %% for code change
            {2, TargetStateUpdate1}
    end,
    %% function to update the target state:
    UpdateTargetStateFun =
        fun(S) ->
            %% update target state where TargetStateUpdate is
            %% update function or new state:
            NewTargetState =
                case is_function(TargetStateUpdate, 1) of
                    true ->
                        TargetStateUpdate(S#state.targetState);
                    false ->
                        TargetStateUpdate
                end,
            S#state{targetState = NewTargetState}
        end,
    NewState = update_all_server_state(State, UpdateTargetStateFun),
    Result = setelement(IndexState, TargetResult, NewState),

    case (element(1, Result) == stop) and (State#state.localServerPidList /= []) of
        true ->
            Fun = fun(Pid) ->
                node(Pid)
            end,
            LocalServerNodeList = lists:map(Fun, State#state.localServerPidList),
            gen_server:multi_call(LocalServerNodeList,
                State#state.name, stopByTarget);
        false ->
            ok
    end,
    Result.

update_all_server_state(State, UpdateFun) ->
    NewState = UpdateFun(State),
    CastFun = fun(Pid) ->
        gen_server:cast(Pid, {update_local_server_state, UpdateFun})
    end,
    lists:foreach(CastFun, State#state.localServerPidList),
    NewState.

try_register_as_global_server(Name, OldGlobalServerPid) ->
    case global:whereis_name(Name) of %% current global server pid in registry
        OldGlobalServerPid ->
            %% sleep up to 1s until old global server pid is deleted
            %% from global registry:
            timer:sleep(random:uniform(1000)),
            try_register_as_global_server(Name, OldGlobalServerPid);
        undefined ->
            %% try to register this process globally:
            global:register_name(Name, self()),
            timer:sleep(100), %% wait before next registration check
            try_register_as_global_server(Name, OldGlobalServerPid);
        NewGlobalServerPid ->
            %% new global server pid (self() or other local server):
            NewGlobalServerPid
    end.
